from flask import Flask, abort, render_template, send_file
import os
import sys

ASSETS_DIR = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__)

def filePaths(relative, directory):
   for filenames in os.listdir(directory):
       yield os.path.join(relative, filenames)

@app.route('/', defaults={'req_path': ''})
@app.route('/<path:req_path>')
def dir_listing(req_path):
    BASE_DIR = sys.argv[1]

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = filePaths(req_path, abs_path)
    def date_for_file(file):
       file_path = os.path.join(BASE_DIR, file)
       date = os.path.getmtime(file_path)
       return -date
    files=sorted(files, key=date_for_file)
    names=map(os.path.basename, files)
    return render_template('files.html', files=zip(files, names))

if __name__ == "__main__":
    context = ('server/cert.pem', 'key.pem')
    app.run(ssl_context=context, host="0.0.0.0", port=5000)
