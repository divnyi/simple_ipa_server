# Installation

Clone the repo. 

Run `./setup_https` to create self-subscribed certificates.

Install Flask. Install whatever lib it screams at when you try to `./run`.

Specify your server url in `server_url` file (without last slash). 

Run `./setup_qr_code`.

## Usage

To run server, just call `./run` (can be double-clicked from Finder).

This will show contents of `./server` folder to the web.

Use `take_build` script to create directory with build artifact and install page.
